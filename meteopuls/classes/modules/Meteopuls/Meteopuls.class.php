<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Rusalex
 * Date: 19.02.14
 * Time: 17:18
 * To change this template use File | Settings | File Templates.
 */
class PluginMeteopuls_ModuleMeteopuls extends Module
{

    /**
     * Инициализируем модуль
     */
    public function Init()
    {

    }

    /**
     * Возвращает массив городов, где ключ - название города, а значение - это его Id
     */
    public function GetCities () {
        $sDateCities = 'http://weather.yandex.ru/static/cities.xml'; // Загружаем xml файл со списком городов и параметров
        $oDateCities=simplexml_load_file($sDateCities);
        $aDateCities=array();
        foreach ($oDateCities->country as $oValue) {



            for ($i=0; $i<($count=$oValue->count()); $i++) {
                $sKey=addslashes($oValue->city[$i]->__toString());
                $oCity=$oValue->city[$i];

                $aDateCities[$sKey]=intval($oCity['id']);
            }
        }
        return $aDateCities;
    }


    /**
     * Возвращаем общий массив данных по всем выбранным городам
     */
    public function GetWeatherForCities ($aCitiesId, $iCol, $aDayOfTheWeek, $aTimeOfDay) {
        $aOutCitiesForecast=array();
        foreach ($aCitiesId as $sCity=>$sCityId) {
            $aOutCitiesForecast[$sCity]=$this->GetWeatherByCityId($sCityId, $iCol, $aDayOfTheWeek, $aTimeOfDay);
        }
        return $aOutCitiesForecast;
    }

    /**
     * Метод обновления данных о погоде в файле.
     */
    public function GetFactWeather($sCityId)
    {
        $sCacheKey = "meteopuls_" . $sCityId;
        if (false === ($aOutFact = $this->Cache_Get($sCacheKey))) {
            $link = 'http://export.yandex.ru/weather-ng/forecasts/' . $sCityId . '.xml';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $link);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 3);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,3);
            $string = curl_exec($ch);
            if (!$string) {
                return false;}
            $oDateCity=simplexml_load_string($string);
        if (!isset($oDateCity)) {
            return false;}
        $aOutFact = array(); // Массив вывода прогноза
        $aOutFact['temperature']=(int)$oDateCity->fact->temperature;
        $aOutFact['image']=(string)$oDateCity->fact->{'image-v3'};
        $aOutFact['weather_type']=(string)$oDateCity->fact->weather_type;
        $aOutFact['country']=(string)$oDateCity['country'];
        $aOutFact['city']=(string)$oDateCity['city'];

        $this->Cache_Set($aOutFact, $sCacheKey, array('cities_url_change'), Config::Get('plugin.meteopuls.meteopuls.cache_lifetime'));

        }
        return $aOutFact;

    }

    /**
     * Загружаем файл прогноза для выбранного города
     * и возвращаем массив данных по этому городу
     */

    private function GetWeatherByCityId($sCityId, $iCol, $aDayOfTheWeek, $aTimeOfDay)
    {

        $link = 'http://export.yandex.ru/weather-ng/forecasts/' . $sCityId . '.xml';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $link);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,3);
        $string = curl_exec($ch);
        if (!$string) {return false;}


        $oDateCity = simplexml_load_string($string);

        $aOutCityForecast = array(); // Массив вывода прогноза
        $iCounter = 0; // Счетчик количества дней, для которых доступен прогноз


        foreach ($oDateCity->day as $oDay) {

            if ($iCounter == $iCol) {
                break;
            }

            $aGetDate = explode("-", $oDay['date']);
            $dtDayOfWeek = date("N", mktime(0, 0, 0, $aGetDate[1], $aGetDate[2], $aGetDate[0]));

            $aOutCityForecast[$iCounter]['day'] = $aGetDate[2];
            $aOutCityForecast[$iCounter]['month'] = $aGetDate[1];
            $aOutCityForecast[$iCounter]['year'] = $aGetDate[0];
            $aOutCityForecast[$iCounter]['day_of_week'] = $aDayOfTheWeek[$dtDayOfWeek];


            for ($i = 0; $i <= 3; $i++) {

                if ($oDay->day_part[$i]->temperature == '') {
                    $nGetTempFrom = $oDay->day_part[$i]->temperature_from;
                    $nGetTempTo = $oDay->day_part[$i]->temperature_to;
                } else {
                    $nGetTempFrom = $oDay->day_part[$i]->temperature - 1;
                    $nGetTempTo = $oDay->day_part[$i]->temperature + 1;
                }
                if ($nGetTempFrom > 0) {
                    $nGetTempFrom = '+' . $nGetTempFrom;
                }
                if ($nGetTempTo > 0) {
                    $nGetTempTo = '+' . $nGetTempTo;
                }
                $aOutCityForecast[$iCounter]['weather'][$i]['temp_from'] = $nGetTempFrom;
                $aOutCityForecast[$iCounter]['weather'][$i]['temp_to'] = $nGetTempTo;
                $aOutCityForecast[$iCounter]['weather'][$i]['image'] = $oDay->day_part[$i]->{'image-v3'};
                $aOutCityForecast[$iCounter]['weather'][$i]['time_of_day'] = $aTimeOfDay[$i];

            }

            $iCounter++;
        }


        return $aOutCityForecast;

    }


}

?>
