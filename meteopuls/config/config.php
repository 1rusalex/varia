<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Rusalex
 * Date: 19.02.14
 * Time: 17:18
 * To change this template use File | Settings | File Templates.
 */
$config=array();
$config['meteopuls']['CityDefault']['Москва'] =27612 ;  // Идентификатор Москвы - по дефолту
$config['meteopuls']['СolDefault'] =2 ; // количество дней default, на сколько нужен прогноз
$config['meteopuls']['DayOfTheWeek'][1] ='понедельник'; // тут и так все понятно
$config['meteopuls']['DayOfTheWeek'][2] ='вторник';
$config['meteopuls']['DayOfTheWeek'][3] ='среда';
$config['meteopuls']['DayOfTheWeek'][4] ='четверг';
$config['meteopuls']['DayOfTheWeek'][5] ='пятница';
$config['meteopuls']['DayOfTheWeek'][6] ='суббота';
$config['meteopuls']['DayOfTheWeek'][7] ='воскресенье';
$config['meteopuls']['TimeOfDay'][0] ='утро';
$config['meteopuls']['TimeOfDay'][1] ='день';
$config['meteopuls']['TimeOfDay'][2] ='вечер';
$config['meteopuls']['TimeOfDay'][3] ='ночь';


$config['table']['city_ip'] = '___db.table.prefix___city_ip';

Config::Set('block.rule_meteopuls',
    array(
//        'path' => '___path.root.web___/*',
        'action' => array('index'),
        'blocks' => array(
            'right'=>array('meteopuls'=>array('params'=>array('plugin'=>'meteopuls'),'priority' => 200))
        )
    )
);
$config['meteopuls']=array(
    'cache_lifetime' => 60 * 30, // 30 minutes
);

return $config;
?>