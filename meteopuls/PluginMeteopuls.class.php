<?php
/*-------------------------------------------------------
*
*   LiveStreet Engine Social Networking
*   Copyright © 2008 Mzhelskiy Maxim
*
*--------------------------------------------------------
*
*   Official site: www.livestreet.ru
*   Contact e-mail: rus.engine@gmail.com
*
*   GNU General Public License, version 2:
*   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
---------------------------------------------------------
*/

/**
 * Запрещаем напрямую через браузер обращение к этому файлу.
 */
if (!class_exists('Plugin')) {
	die('Hacking attempt!');
}

class PluginMeteopuls extends Plugin {



	/**
	 * Активация плагина "Meteopuls".
	 * Создание таблицы в базе данных при ее отсутствии.
	 */
	public function Activate() {

        parent::Activate();
        if (!$this->isTableExists('prefix_city_ip')) {
            /**
             * При активации выполняем SQL дамп
             */
            if (count($this->ExportSQL(dirname(__FILE__).'/dump.sql'))>0) {
                $aCities=$this->PluginMeteopuls_Meteopuls_GetCities();

                if (count($aCities)>0) {
                    $i=1; //итератор для вычисления последнего значения массива
                    $sSQL='INSERT INTO prefix_city_ip (city_id, city_name) VALUES';
                    foreach ($aCities as $sName=>$sCityId) {
                        $sSQL.='(\''.$sCityId.'\', \''.$sName.'\')';
                        if ($i!=count($aCities)) {  //пока весь массив, кроме последнего значения не исчерпан, добавляем запятую, после каждой группы значений
                            $sSQL.=',';}
                        $i++;
                    }
                    $sSQL.=';';
                    $this->ExportSQLQuery($sSQL);
                }
            };
        }
        return true;
	}

public function Deactivate () {
    parent::Deactivate();
    if ($this->isTableExists('prefix_city_ip')) {
        $sSQL='DROP TABLE IF EXISTS prefix_city_ip';
        $this->ExportSQLQuery($sSQL);
    }
    return true;
}

    /**
	 * Инициализация плагина
	 */
	public function Init() {

	}
}
?>