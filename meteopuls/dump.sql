CREATE TABLE IF NOT EXISTS `prefix_city_ip` (
  `city_ip_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `city_id` int(11) unsigned DEFAULT NULL,
  `city_name` varchar(20) NOT NULL,
  PRIMARY KEY (`city_ip_id`),
  KEY `city_name` (`city_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

